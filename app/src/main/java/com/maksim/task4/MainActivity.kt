package com.maksim.task4

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

/*
*This activity set image in $image_from_gallery from $load_button
* logic button this(fun getIntentAndResult())
* logic image_from_gallery in override fun OnActivityForResult
*
*@author Maksim Larchenko
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        load_button.setOnClickListener {
            getIntentAndResult()
        }
    }

    private fun getIntentAndResult() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = imageType
        startActivityForResult(intent, REQUEST_PICTURE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_PICTURE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            val media = MediaStore.Images.Media.getBitmap(contentResolver, uri)
            image_from_gallery.setImageBitmap(media)
        } else Toast.makeText(this, getString(R.string.didntLoad), Toast.LENGTH_SHORT).show()
    }

    private companion object {
        private val REQUEST_PICTURE = 1
        private val imageType = "image/*"
    }
}
